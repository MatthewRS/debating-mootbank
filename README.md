# Debating Moot Sharing Site

A website I made in level 3 digital technology class, in year 13 (2019). 
Made using flask and jinja (as far as I recall in 2024). 

To run:
- Enter the venv
- Duplicate configMainSite Example.py, remove " Example" from its name, and change the secret key and config if you want
- Run ```python app.py``` (may require installing some python libs) 
