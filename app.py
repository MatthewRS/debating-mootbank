from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify
from passlib.hash import sha256_crypt
import time
from datetime import datetime
import math

from configMainSite import Config
import classes.forms
import classes.moots
import classes.user
import classes.votes

app = Flask(__name__)
config = Config()


# session['logged_in'] = False
# session['username'] = ""
# session.init_app(app)

@app.errorhandler(404) # The 404 error page
def page_not_found(e):
	return render_template('errors/404.html'), 404

# --- This is a pain, I'm trying to implement a history feature but it IS working YAYYYYY ---
@app.before_request
def store_history(): # Set the user's last few pages into a history session var, so I can redirect to previous pages. For use in the deletion and editing links
	global session
	# print("hi")
	# print("\n\n\n", i)
	

	if session.get("history") != None:
		# print("\n\n\nSession History before:", session["history"])
		historyDict = session["history"]

		# Keep out checks for CSS and casting votes
		if request.url.find("/static/") == -1 and request.url.find("/_vote") == -1:
			historyDict.append(request.url)
		
		# Keep the history shortish, it doesn't need to be super long
		if len(historyDict) > 10:
			del historyDict[0]

		session["history"] = historyDict

		
	else:
		session["history"] = []
		# a=1
	
	# print("Session History:", session["history"])


# @app.template_filter('ctime') # Copied from the internet, in html use '{{item.post_time | ctime}}' to convert timestamp to human-readable time
# def timectime(s):
# 	return time.ctime(s) # datetime.datetime.fromtimestamp(s)

@app.template_filter('getUserName') # A cosmetic filter so I can display a user's name in the jinja
def getUser(inID):
	user = classes.user.User()
	# print("\n\n\n", inID)
	return user.getUser("ID", int(inID))["user_name"]

@app.template_filter('getMootScore') # A cosmetic filter so I can display a moot's score in the jinja
def getMootScore(inID):
	votes = classes.votes.Votes()
	# print("\n\n\n", inID)
	return round(votes.getMootScore(inID), 2)

@app.template_filter('getHumanTime') # A cosmetic filter so I can display a moot's posting time in a human-readable format, instead of UNIX time
def getHumanTime(inTime):
	if datetime.fromtimestamp(inTime).strftime('%Y %m %d') == datetime.fromtimestamp(time.time()).strftime('%Y %m %d'): # if the moot was posted today
		dateStr = datetime.fromtimestamp(inTime).strftime('at %H.%M today ')
	elif datetime.fromtimestamp(inTime).strftime('%Y') == datetime.fromtimestamp(time.time()).strftime('%Y'): # if the moot was posted this year
		dateStr = datetime.fromtimestamp(inTime).strftime('at %H.%M on %d %B ')
	else: # if the moot was not posted this year
		dateStr = datetime.fromtimestamp(inTime).strftime('at %H.%M on %d/%m/%Y ')
	# dateStr = datetime.fromtimestamp(inTime).strftime('at %H.%M.%S on %d/%m/%Y ')
	# print(dateStr)
	return dateStr



@app.template_filter('getVoteSingle') # Get the vote a given user cast on a moot
def getVoteSingle(userID, mootID):
	votes = classes.votes.Votes()
	# print("\n\n\n", inID)
	return votes.getVote(userID, mootID)

@app.template_filter('getUserVote') # Get the vote of the current user. Used to show a user what score they gave moots in the displayed list
def getUserVote(mootID):
	votes = classes.votes.Votes()
	user = classes.user.User()
	# print("\n\n\n", inID)
	if session.get('username') != None:
		# print("\n\n\n'", session.get('username'), "'")
		return votes.getVote(user.getUser("username", session.get('username'))["user_id"], mootID)
	else:
		return -1

@app.template_filter('getNumMootsFromUser') # Get the number of moots any user has posted
def getNumMootsFromUser(userID):
	moots = classes.moots.Moots()
	userMoots = moots.grabMoots("user", userID, 0, 999999999999999999999999)
	# print(userMoots)
	if userMoots != False:
		userNumMoots = len(userMoots)
	else:
		userNumMoots = 0
	
	return userNumMoots
	# userArray.append([userNumMoots, i])

@app.template_filter('getUserCanDelete') # Get whether the current user can delete a moot, whether they're an admin or the user that posted it
def getUserCanDelete(mootID):
	moots = classes.moots.Moots()
	user = classes.user.User()
	result = False

	if session.get("username"):
		if moots.grabMoots("ID", mootID, 0, 999999999999999999999999)["moot_owner"] == user.getUser("username", session.get("username"))["user_id"]:
			result = True
			
		if user.getUser("username", session.get("username"))["user_admin"] == 1:
			result = True


	return result


@app.template_filter('getUserAdmin') # Return true if a given user (by username) is an admin. Used in the user list page
def getUserAdmin(userName):
	# moots = classes.moots.Moots()
	user = classes.user.User()
	result = False
		
	if user.getUser("username", session.get("username")) != False:
		if user.getUser("username", session.get("username"))["user_admin"] == 1:
			result = True
	
	return result


@app.route("/_vote") # ajax stuff, from http://flask.pocoo.org/docs/1.0/patterns/jquery/
def voteRoute(): # used for vasting votes with JS
	# print("\n\n\nThe user tried to vote on a moot")
	userInVote = request.args.get('voteScore', 0, type=str)
	mootID = request.args.get('mootID', 0, type=int)

	# print("\n\n\n", mootID, userInVote)

	votes = classes.votes.Votes()
	user = classes.user.User()

	# print(session.get('username'))
	if session.get('username') != None: # If the user is logged in
		userID = user.getUser("username", session.get('username'))["user_id"]

		DBresult = votes.addVote(mootID, userID, userInVote)
		if DBresult == True:
			DBresult = round(votes.getMootScore(mootID), 2)
		else:
			DBresult = "Vote Failed"
	else: 
		# DBresult = votes.getMootScore(mootID)
		# DBresult = "Please log in to vote (Also, Matt pls improve this message)"
		DBresult = -1*votes.getMootScore(mootID)
	
	

	# return "IDK what I should put in the return statement"
	# return result
	return jsonify(result=DBresult)



# @app.route("/_delete") # ajax stuff, from http://flask.pocoo.org/docs/1.0/patterns/jquery/
# def deleteMootRoute():
# 	# print("\n\n\nThe user tried to vote on a moot")
# 	userInVote = request.args.get('voteScore', 0, type=str)
# 	mootID = request.args.get('mootID', 0, type=int)

# 	# print("\n\n\n", mootID, userInVote)

# 	votes = classes.votes.Votes()
# 	user = classes.user.User()

# 	userID = user.getUser("username", session.get('username'))["user_id"]
	
# 	DBresult = votes.addVote(mootID, userID, userInVote)
# 	if DBresult == True:
# 		DBresult = "Submitted!"
# 	else:
# 		DBresult = "Vote Failed"

# 	# return "IDK what I should put in the return statement"
# 	# return result
# 	return jsonify(result=DBresult)





# @app.route("/ajax") # TEMP testing page for ajax
# def ajaxPage():
	# return render_template("ajaxTesting.html")

@app.route("/")
def homePage():
	# print("\n\n\n", session)
	# print(session.get("history"))
	return render_template("home.html")

# @app.route("/customURL/<string:name>")
# def customURL(name):
# 	return render_template("customURL.html", inStr = name)

@app.route("/login", methods = ["GET", "POST"])
def loginPage():

	# If logged in, redirect the user to the home page
	username = session.get('username')
	if username:
		return redirect(url_for("newMootsPage"))


	loginForm = classes.forms.LoginForm(request.form)

	if request.method == "POST" and loginForm.validate():
		username = loginForm.username.data
		password = str(loginForm.password.data)
		# print("\n\n", loginForm.password.data, "\n")


		# print(name, password)
		user = classes.user.User()

		result = user.authUser(username, password)
		# print("-----------\n\n", name)
		if result:
			flash("Logged in as " + str(result), category="success") # use the result string, because it will have the correct capitalisation

			session['logged_in'] = True
			session['username'] = result
			session["history"] = []


			return redirect(url_for("newMootsPage"))

		else:
			flash("Username or password is incorrect", category="danger")
			return redirect(url_for("loginPage"))

	else:
		return render_template("login.html", form = loginForm)


@app.route("/register", methods = ["GET", "POST"]) # Get pulls from the server, post sends to the server
def registerPage():

	# If logged in, redirect the user to the home page
	username = session.get('username')
	if username:
		return redirect(url_for("newMootsPage"))


	registerForm = classes.forms.RegisterForm(request.form)

	if request.method == "POST" and registerForm.validate():
		username = registerForm.username.data
		password = str(registerForm.password.data)
		
		user = classes.user.User()

		result = user.registerUser(username, password)
		

		if result == "nameInUse":
			flash("That username is already taken", category="danger")
			return redirect(url_for("registerPage"))
		elif result == "success":
			flash("Successfully registered!", category="success")

			session['logged_in'] = True
			session['username'] = username
			return redirect(url_for("newMootsPage"))
		else:
			flash("Something went wrong while registering", category="danger")
			return redirect(url_for("registerPage"))

			

	else:
		return render_template("register.html", form = registerForm)

@app.route("/logout")
def logoutPage():
	session['logged_in'] = False
	session['username'] = None
	session["history"] = []

	flash("Logged Out", category="success")
	return redirect(url_for("loginPage"))


@app.route("/submit", methods = ["GET", "POST"])
def submitPage():

	# If not logged in, redirect the user to the login page
	username = session.get('username')
	if not username:
		return redirect(url_for("loginPage"))
	

	submitForm = classes.forms.SubmitForm(request.form)

	if request.method == "POST" and submitForm.validate():
		title = submitForm.title.data
		body = submitForm.body.data

		moots = classes.moots.Moots()

		result = moots.createMoot(session.get('username'), title, body.strip())

		if result:
			flash("Posted!", "success")
			return redirect(url_for("newMootsPage"))
		else:
			flash("Something went wrong :(", "danger")
			return redirect(url_for("submitPage"))
		
		
		
	else:
		if session["logged_in"] == True:
			return render_template("submit.html", form = submitForm)
		else:
			return redirect(url_for(loginPage))


@app.route("/moots/new")
def newMootsPage():

	rangeStart = 0
	rangeEnd = config.mootShowNum

	moots = classes.moots.Moots()
	showList = moots.grabMoots("new", "", rangeStart, rangeEnd)

	totalPages = math.ceil((len(moots.grabMoots("new", "", 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 

	# print("\n\n\n", request.path, "\n\n")

	return render_template("listMoots.html", showTitle = "New Moots", showList = showList, currentUsername = session.get('username'), currentPage = 1, maxPages = totalPages)

@app.route("/moots/new/page<int:pageNum>")
def newMootsPageNumbered(pageNum):
	pageNum = int(pageNum)

	rangeStart = config.mootShowNum * (pageNum - 1)
	rangeEnd = config.mootShowNum * pageNum

	moots = classes.moots.Moots()
	showList = moots.grabMoots("new", "", rangeStart, rangeEnd)

	totalPages = math.ceil((len(moots.grabMoots("new", "", 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 

	if pageNum > totalPages:
		return redirect("/moots/new/page" + str(totalPages))
	if pageNum == 1:
		return redirect("/moots/new")

	# print("\n\n\n", request.path, "\n\n")

	# print("\n\n", rangeStart, rangeEnd, pageNum, totalPages, showList)
	return render_template("listMoots.html", showTitle = "New Moots", showList = showList, currentUsername = session.get('username'), currentPage = pageNum, maxPages = totalPages)


@app.route("/moots/top")
def topMootsPage():

	rangeStart = 0
	rangeEnd = config.mootShowNum

	moots = classes.moots.Moots()
	showList = moots.grabMoots("score", "", rangeStart, rangeEnd)

	# if moots.grabMoots("score", "", 0, -1):
	totalPages = math.ceil((len(moots.grabMoots("score", "", 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 
	# else:
	# 	totalPages = 1

	return render_template("listMoots.html", showTitle = "Top Scoring Moots", showList = showList, currentUsername = session.get('username'), currentPage = 1, maxPages = totalPages)

@app.route("/moots/top/page<int:pageNum>")
def topMootsPageNumbered(pageNum):

	rangeStart = config.mootShowNum * (pageNum - 1)
	rangeEnd = config.mootShowNum * pageNum

	moots = classes.moots.Moots()
	showList = moots.grabMoots("score", "", rangeStart, rangeEnd)
	# print(showList)

	totalPages = math.ceil((len(moots.grabMoots("score", "", 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 

	if pageNum > totalPages:
		return redirect("/moots/top/page" + str(totalPages))
	if pageNum == 1:
		return redirect("/moots/top")

	# print("\n\n", rangeStart, rangeEnd, pageNum, totalPages, showList)

	return render_template("listMoots.html", showTitle = "Top Scoring Moots", showList = showList, currentUsername = session.get('username'), currentPage = pageNum, maxPages = totalPages)



@app.route("/moots/user/<string:username>")
def userMootsPage(username):

	rangeStart = 0
	rangeEnd = config.mootShowNum

	moots = classes.moots.Moots()
	user = classes.user.User()
	userID = user.getUser("username", username)
	if userID:
		userID = userID["user_id"] # if the user does exist, only get its ID
	# print("\n\n\n", userID)

	showList = moots.grabMoots("user", userID, rangeStart, rangeEnd)

	totalPages = math.ceil((len(moots.grabMoots("user", userID, 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 
	# print(showList)

	# print(user.getUser("ID", userID))
	postingUser = user.getUser("ID", userID)
	if postingUser:
		showTitle = "Moots by " + str(postingUser["user_name"])

		return render_template("listMoots.html", showTitle = showTitle, showList = showList, currentUsername = session.get('username'), currentPage = 1, maxPages = totalPages)
	else:
		flash("That user does not exist", "warning")
		
		# redir to last page
		redirURL = session["history"][-2]
		return redirect(redirURL)

@app.route("/moots/user/<string:username>/page<int:pageNum>")
def userMootsPageNumbered(username, pageNum):

	rangeStart = config.mootShowNum * (pageNum - 1)
	rangeEnd = config.mootShowNum * pageNum

	moots = classes.moots.Moots()
	user = classes.user.User()
	userID = user.getUser("username", username)
	if userID:
		userID = userID["user_id"] # if the user does exist, only get its ID
	# print("\n\n\n", userID)

	showList = moots.grabMoots("user", userID, rangeStart, rangeEnd)

	totalPages = math.ceil((len(moots.grabMoots("user", userID, 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 
	# print(showList)

	if pageNum > totalPages:
		return redirect("/moots/user/" + username + "/page" + str(totalPages))
	if pageNum == 1:
		return redirect("/moots/user/" + username)


	# print(user.getUser("ID", userID))
	postingUser = user.getUser("ID", userID)
	if postingUser:
		showTitle = "Moots by " + str(postingUser["user_name"])

		return render_template("listMoots.html", showTitle = showTitle, showList = showList, currentUsername = session.get('username'), currentPage = pageNum, maxPages = totalPages)
	else:
		flash("That user does not exist", "warning")
		
		# redir to last page
		redirURL = session["history"][-2]
		return redirect(redirURL)


@app.route("/moots/delete/<int:mootID>")
def deleteMootPage(mootID):

	# If not logged in, redirect the user to the login page
	username = session.get('username')
	if not username:
		return redirect(url_for("loginPage"))

	# rangeStart = 0 # this is hopefully temporary
	# rangeEnd = -1 # this is hopefully temporary

	moots = classes.moots.Moots()

	result = moots.deleteMoot(session.get('username'), mootID)

	if result:
		flash("Moot deleted!", "success")

		# redir to last page
		redirURL = session["history"][-2]
		return redirect(redirURL)
	else:
		flash("Moot could not be deleted", "danger")
		# redir to last page
		redirURL = session["history"][-2]
		return redirect(redirURL)


@app.route("/moots/edit/<int:mootID>", methods = ["GET", "POST"])
def editPage(mootID):

	# If not logged in, redirect the user to the login page
	username = session.get('username')
	if not username:
		return redirect(url_for("loginPage"))
	
	moots = classes.moots.Moots()
	user = classes.user.User()

	if moots.getMootOwner(mootID) != user.getUser("username", session.get('username'))["user_id"]:
		flash("Cannot edit that moot", "danger")
		
		# redir to last page
		redirURL = session["history"][-2]
		return redirect(redirURL)

	submitForm = classes.forms.SubmitForm(request.form)

	if request.method == "POST" and submitForm.validate():
		title = submitForm.title.data
		body = submitForm.body.data

		# result = moots.createMoot(session.get('username'), title, body)
		result = moots.editMoot(session.get('username'), mootID, title, body.strip())

		if result:
			flash("Edited!", "success")
			
			# redir to last page
			redirURL = session["history"][-3]
			return redirect(redirURL)
		else:
			flash("Cannot edit that moot", "danger")
			
			# redir to last page
			redirURL = session["history"][-2]
			return redirect(redirURL)
		
		
		
	else:
		if session["logged_in"] == True:

			oldMoot = moots.grabMoots("ID", mootID, 0, 999999999999999999999)
			# print("\n\n\n", oldMoot)

			submitForm.title.data = oldMoot["moot_title"]
			submitForm.body.data = oldMoot["moot_body"]

			return render_template("submit.html", form = submitForm)
		else:
			# flash("You must be logged in!", "danger")
			return redirect(url_for(loginPage))



# @app.route("/user/<string:user>") # Get pulls from the server, post sends to the server
# def postListUserPage(user):

# 	dbHandler = classes.user.DBhandler()
	
# 	# print("-----------\n\n", result)
# 	currentUser = dbHandler.getUser(session.get('username'))
# 	if currentUser:
# 		currentUser = currentUser["user_name"]
# 	else:
# 		currentUser = ""
	
# 	show = dbHandler.getPosts("username", user)

# 	# print(userID)
# 	if show:
# 		return render_template("listPosts.html", show = show, user = currentUser)
# 	else:
# 		flash("Either that user doesn't exist, or they have not posted", "warning")
# 		return redirect(url_for("postListPage"))


# @app.route("/deletePost/<string:postID>")
# def deletePostPage(postID):
# 	dbHandler = classes.user.DBhandler()

# 	result = dbHandler.deletePost(postID, session.get('username'))

# 	if result:
# 		flash("Post deleted!", category="success")
# 		redirURL = "/user/" + str(session.get('username'))
# 		return redirect(redirURL)
# 	else:
# 		flash("Couldn't delete that post", category="danger")
# 		return redirect(url_for("homePage"))

# 	return postID

# @app.route("/edit/<string:postID>", methods = ["GET", "POST"])
# def editPostPage(postID):
# 	dbHandler = classes.user.DBhandler()
# 	tagHandler = TagHandler()

# 	editPostForm = classes.formHandler.EditPostForm(request.form)

# 	if request.method == "POST" and editPostForm.validate():
# 		# edit the post in the database

# 		newBody = editPostForm.body.data
# 		newTags = editPostForm.tags.data.strip().strip(",")
# 		result = dbHandler.editPost(postID, session.get('username'), newBody, newTags)

# 		if result:
# 			flash("Edit succesful!", "success")
# 		else:
# 			flash("Edit failed :(", "danger")
# 		# redirURL = "/user/" + str(session.get('username'))
# 		redirURL = "/new"
# 		return redirect(redirURL)
# 	else:
# 		# show the editing form
# 		oldPost = dbHandler.getPosts("postID", postID)
# 		if oldPost:
# 			if dbHandler.getUserPost(postID)["user_name"] == session.get('username'): # ask these ifs one after the other, becasue otherwise a nonexistent post would throw an error when trying to get its posting user
# 				# print(oldPost)

# 				editPostForm = classes.formHandler.EditPostForm(request.form)
# 				editPostForm.body.data = oldPost["post_body"]

# 				# print("\n\n\n", postID)

# 				oldTagsRaw = tagHandler.getTagsPost(postID)
# 				oldTagsStr = ""
# 				for i in oldTagsRaw:
# 					oldTagsStr += str(i["tag_name"]) + ", "

# 				editPostForm.tags.data = oldTagsStr.strip(", ")
# 				# newBody = editPostForm.body.data


# 				# result = dbHandler.editPost(postID, session.get('username'), newBody)
# 				tagHandler = TagHandler()
# 				# allTags = tagHandler.
# 				allTags = tagHandler.getTagsAll()[:15]

# 				return render_template("editPost.html", form = editPostForm, oldPost = oldPost, allTags = allTags)
# 			else:
# 				flash("You can't edit that!", category="danger") # the post belongs to another user
# 				return redirect(url_for("homePage"))
# 		else:
# 			flash("You can't edit that!", category="danger") # the post doesn't exist
# 			return redirect(url_for("homePage"))



@app.route("/search", methods = ["GET", "POST"]) # The URL you're sent to when you hit 'search'
def searchFromBox():
	if request.method == "POST":

		searchForm = classes.forms.SearchForm(request.form) # IDK if I need this, it seems to work without but I'm scared to remove it because it seems important

		# searchStr = searchForm.query.data
		searchStr = str(request.form["query"])
		if searchStr != "":
			# print(searchStr)

			redirURL = "/moots/search/" + searchStr

			return redirect(redirURL)
		else:
			# redir to last page
			redirURL = session["history"][-2]
			return redirect(redirURL)

	else:
		return redirect(url_for("homePage"))


@app.route("/moots/search/<string:query>") # The page that actually displays your search results
def searchPage(query):
	rangeStart = 0
	rangeEnd = config.mootShowNum

	
	moots = classes.moots.Moots()
	showList = moots.grabMoots("search", query, rangeStart, rangeEnd)

	# if moots.grabMoots("search", query, 0, -1)
	totalPages = math.ceil((len(moots.grabMoots("search", query, 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 

	showTitleString = "Search Results for '" + query + "'"

	return render_template("listMoots.html", showTitle = showTitleString, showList = showList, currentUsername = session.get('username'), currentPage = 1, maxPages = totalPages)

@app.route("/moots/search/<string:query>/page<int:pageNum>") # The page that actually displays your search results
def searchPageNumbered(query, pageNum):
	rangeStart = config.mootShowNum * (pageNum - 1)
	rangeEnd = config.mootShowNum * pageNum

	
	moots = classes.moots.Moots()
	showList = moots.grabMoots("search", query, rangeStart, rangeEnd)

	totalPages = math.ceil((len(moots.grabMoots("search", query, 0, -1)) + 1) / config.mootShowNum) # Length going to -1 means it will take all moots bar the last. Adding 1 onto that means it'll get the total number of moots in the system, more reliably than if I used a placeholder like 99999999999999999 

	if pageNum > totalPages:
		return redirect("/moots/search/" + query + "/page" + str(totalPages))
	if pageNum == 1:
		return redirect("/moots/search/" + query)

	showTitleString = "Search Results for '" + query + "'"

	return render_template("listMoots.html", showTitle = showTitleString, showList = showList, currentUsername = session.get('username'), currentPage = pageNum, maxPages = totalPages)



@app.route("/users")
def allUsersPage():

	# rangeStart = 0 # this is hopefully temporary
	# rangeEnd = -1 # this is hopefully temporary

	user = classes.user.User()
	moots = classes.moots.Moots()
	userList = user.getAllUsers()

	userArray = []
	showList = []

	for i in userList:
		# print(i["user_name"])
		userMoots = moots.grabMoots("user", i["user_id"], 0, 999999999999999999999999)
		# print(userMoots)
		if userMoots != False:
			userNumMoots = len(userMoots)
		else:
			userNumMoots = 0
		
		userArray.append([userNumMoots, i])

	userArray = sorted(userArray,key=lambda x: (x[0])) # sort by the first collumn (score)
	userArray = userArray[::-1] # invert order of the list

	for i in userArray:
		showList.append(i[1])

	# print(userArray)
	# print(showList)
	# showList = userList

	return render_template("listUsers.html", showList = showList, currentUsername = session.get('username'))


@app.route("/user/promote/<int:userID>") # The button you click to promote a user, found on the user list
def promoteUserPage(userID):
	user = classes.user.User()
	result = True

	targetUser = user.getUser("ID", userID)
	currentUser = user.getUser("username", session.get("username"))

	# print("\n\n\n", targetUser)
	if targetUser == False:
		flash("Could not promote that user", "danger")
		return redirect(url_for("allUsersPage"))
	if targetUser["user_admin"] == 1:
		result = False
	if currentUser["user_admin"] == 0:
		result = False

	if result == True:
		user.changeUserPrivs(userID, 1)
		flash("User promoted!", "success")
		return redirect(url_for("allUsersPage"))
	else:
		flash("Could not promote that user", "danger")
		return redirect(url_for("allUsersPage"))


@app.route("/user/demote/<int:userID>") # Press button, demote user
def demoteUserPage(userID):
	user = classes.user.User()
	result = True

	targetUser = user.getUser("ID", userID)
	currentUser = user.getUser("username", session.get("username"))

	# print("\n\n\n", targetUser)
	if targetUser == False:
		flash("Could not demote that user", "danger")
		return redirect(url_for("allUsersPage"))
	if targetUser["user_admin"] == 0:
		result = False
	if currentUser["user_admin"] == 0:
		result = False
	if targetUser["user_id"] == currentUser["user_id"]:
		result = False

	if result == True:
		user.changeUserPrivs(userID, 0)
		flash("User demoted", "success")
		return redirect(url_for("allUsersPage"))
	else:
		flash("Could not demote that user", "danger")
		return redirect(url_for("allUsersPage"))
	



if __name__ == "__main__": #Only go into debug mode if it's being run properly, not if another program is remotely running it
	app.secret_key = config.secret_key #Set a secret key so only the browser that made the request can recieve it
	app.run(debug=True)
