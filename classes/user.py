import sqlite3
# from classes.tags import TagHandler
from configMainSite import Config
from passlib.hash import sha256_crypt
import time

config = Config()

class User():
	def __init__(self):
		self.dbPath = config.dbPath # path is relitave from app.py
		# print("------- dbPath =", self.dbPath)



	def registerUser(self, username, password): # Register a new user, taking the user's desired name and unhashed password
		hashedPassword = sha256_crypt.encrypt(password) # Get a hash of the password

		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()
			
			# Check if that user has already registered
			cur.execute("SELECT * FROM users WHERE user_name = ? COLLATE NOCASE", [username])
			if cur.fetchone(): # if a user by the same name exists (ignoring case)
				return "nameInUse"
			else: # If that username is free
				result = cur.execute("INSERT INTO users (user_name, user_password, user_register_time, user_last_login_time) VALUES (?, ?, ?, ?)", (username, hashedPassword, time.time(), time.time())) # Insert into the DB the new user

				if result: # if the entry was successful
					return "success"
				else:
					return "DBerror"
	
	def authUser(self, inUsername, inPassword): # Check if a username (case insensitive) and unhashed password match one from the DB
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			# Try to grab that user, if it exists (ignoring case)
			cur.execute("SELECT * FROM users WHERE user_name = ? COLLATE NOCASE", [inUsername])
			result = cur.fetchone()

			

			# print("\n\n\n", result)
			if result: # If that user exists
				if result["user_name"].lower() == inUsername.lower() and sha256_crypt.verify(inPassword, result["user_password"]): # if the username is the same as the inputted one (ignoring case) and the password verifies
					

					cur.execute("UPDATE users SET user_last_login_time = ? WHERE user_id = ?", (time.time(), result["user_id"])) # Update the user's last login time. Not displayed on the user list, but helpful for moderation

					conn.commit()
					cur.close()

					return result["user_name"] # If successful, return the user's name from the DB, which contains the correct capitalisation. This means users don't need to be case-sensitive when they log in, and everything will still work
				else:
					return False
			else:
				return False

	def getUser(self, grabType, grabData): # Get a user from the DB, either by ID or username (exact match). This is used by the system when it needs to deal with users other than the one currently logged in. The username option is because sometimes that's easier to get then the user ID, depending on the circumstance
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			if grabType == "ID":
				# print("\n\n\n", grabData)
				cur.execute("SELECT * FROM users WHERE user_id = ?", [grabData])
			elif grabType == "username":
				cur.execute("SELECT * FROM users WHERE user_name = ?", [grabData])
			else: # If grabType wasn't one of the two correct inputs
				return False
			
			result = cur.fetchone()
			conn.commit()
			cur.close()

			if result:
				return result
			else:
				return False
	
	def getAllUsers(self): # Get a list of all users, sorted by registration time. Used by the all users page, not much else to say
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			cur.execute("SELECT * FROM USERS ORDER BY user_register_time DESC")
			
			result = cur.fetchall()
			conn.commit()
			cur.close()

			if result:
				return result
			else:
				return False
	
	def changeUserPrivs(self, userID, action): # Used when promoting/demoting a user. I use one function for both as they're pretty similar actions. Note I don't do any authentication in this function, as I already check if the current user has permission to do this in app.py, when the user clicks the 'promote'/'demote' button
		# action: 1 = promote, 0 = demote
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			cur.execute("UPDATE users SET user_admin = ? WHERE user_id = ?", [action, userID])
			
			result = cur.fetchall()
			conn.commit()
			cur.close()

			if result:
				return result
			else:
				return False

			