from wtforms import Form, validators, StringField, TextAreaField, PasswordField, TextAreaField

class RegisterForm(Form): # registration form, no email as I wouldn't check it anyway
	username = StringField("Username", [validators.Length(min = 4, max = 20)])
	password = PasswordField("Password", [
		validators.DataRequired(), 
		validators.EqualTo("confirmPassword", message = "Passwords don't match")
	])
	confirmPassword = PasswordField("Confirm Password")


class LoginForm(Form): # the login page form
	username = StringField("Username", [validators.DataRequired()])
	password = PasswordField("Password", [validators.DataRequired()])


class SubmitForm(Form): # for when creating/editing a moot
	title = StringField("Motion", [validators.DataRequired()])
	body = TextAreaField("Extra information")
	# tags = StringField("Tags")
	
# class EditPostForm(Form):
# 	body = TextAreaField("Message")
# 	tags = StringField("Tags")

class SearchForm(Form): # for the searchbar
	query = StringField("query",[validators.Length(min=1),validators.DataRequired()])

