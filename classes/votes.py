import sqlite3
# from classes.tags import TagHandler
from configMainSite import Config
import classes.moots
import classes.user

config = Config()

class Votes():
	def __init__(self):
		self.dbPath = config.dbPath # path is relitave from app.py
		# print("------- dbPath =", self.dbPath)

	def addVote(self, mootID, userID, voteScore): # Cast a vote, from a user, on a moot. Delete any existing vote already, to prevent double-voting
		# user = classes.user.User()
		moots = classes.moots.Moots()

		# print("\n\n\n", userID)
		# try: 
		# voteScore = int(voteScore) # Just in case a float is passed in
		# except:
		# 	# print("\n\n\n")
		# 	return False # If the program passes in a non-number for some reason. It shouldn't happen, but this legacy code is from when the vote button was an input box
		
		# voteScore = round(voteScore)

		
		with sqlite3.connect(self.dbPath) as conn: # Setup DB connection
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			self.removeVote(userID, mootID) # This runs the 'remove vote' method. It returns true whether or not there was a vote to remove, but it doesn't matter whether there *was* a vote, just that there's not one when we try to cast a new vote under the same user and moot
			# if result == False:
			# 	return False
			
			if not moots.grabMoots("ID", mootID, 0, 0): # If the moot doesn't exist (if something is wrong, perhaps the user is fiddling with the JS) stop before you cast a vote on a moot that doesn't exist
				return False

			cur.execute("INSERT INTO voteLinker (user_id, moot_id, vote_score) VALUES (?, ?, ?)", (userID, mootID, voteScore))

			conn.commit()
			cur.close()

			return True
	
	def removeVote(self, userID, mootID): # Remove the vote a given user cast on a given moot. Primarily for when a user cahnges their vote

		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			# print("\n\n\n", userID, mootID)
			cur.execute("SELECT * FROM voteLinker WHERE user_id = ? AND moot_id = ?", (userID, mootID))
			result = cur.fetchone()
			if result:
				cur.execute("DELETE FROM voteLinker WHERE user_id = ? AND moot_id = ?", (userID, mootID))
			
			conn.commit()
			cur.close()

			return True

	def getMootScore(self, mootID): # Get the mean score of a given moot
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			cur.execute("SELECT * FROM voteLinker WHERE moot_id = ?", [mootID]) # Get a list of all votes
			result = cur.fetchall()

			conn.commit()
			cur.close()

			totalScore = 0
			for i in result: # Get the sum of all votes
				totalScore += i["vote_score"]
			
			if len(result) != 0: # Divide by the number of votes
				avgScore = totalScore/len(result)
				
				return avgScore
			else:
				return 0 # Return a score of 0 if no vote has been cast on the moot
	
	def getVote(self, userID, mootID): # Get the vote a user cast on a moot, used for displaying what score a user posted in the big ol list
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			cur.execute("SELECT * FROM moots WHERE moot_id = ?", [mootID]) # Throw a fit if that moot doesn't exist
			result = cur.fetchone()
			if result == None:
				return False
			
			cur.execute("SELECT * FROM voteLinker WHERE user_id = ? AND moot_id = ?", (userID, mootID)) # Get that vote, if it exists
			result = cur.fetchone()

			if result != None:
				return result["vote_score"]
			else:
				return 0 # Return a score of 0 if no vote has been cast

			conn.commit()
			cur.close()

			