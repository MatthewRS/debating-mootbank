import sqlite3
# from classes.tags import TagHandler
from configMainSite import Config
from passlib.hash import sha256_crypt
import time
import classes.user
import classes.votes

config = Config()

class Moots():
	def __init__(self):
		self.dbPath = config.dbPath # path is relitave from app.py
		# print("------- dbPath =", self.dbPath)


	def createMoot(self, username, title, body): # create a moot in the DB, given the current user and moot info
		# print("\n\n", title, body, username)
		
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			user = classes.user.User()
			userID = user.getUser("username", username)
			# print(userID)
			userID = userID["user_id"]

			result = cur.execute("INSERT INTO moots (moot_owner, moot_edited, moot_title, moot_body, moot_post_time) VALUES (?, 0, ?, ?, ?)", (userID, title, body, time.time()))
			# print(userID, title, body)
			
			conn.commit()
			cur.close()

			if result:
				return True
			else:
				return False
		

	def grabMoots(self, grabType, grabData, rangeStart, rangeEnd): # grab moots given a criteria. grabType which rule to follow, and grabData gives the criteria
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()


			if grabType == "ID": # get one moot, given its ID
				cur.execute("SELECT * FROM moots WHERE moot_id = ?", [grabData])
				result = cur.fetchone()
				# print("\n\n", result)

			elif grabType == "new": # get all moots, sotring by time posted
				cur.execute("SELECT * FROM moots ORDER BY moot_post_time DESC")
				result = cur.fetchall()
				if rangeEnd > 0:
					result = result[rangeStart:rangeEnd] # get only the selected moots
				else:
					result = result[rangeStart:99999999999999]

			elif grabType == "score": # grab all moots, sort in python instead of using sqlite because it's easier
				votes = classes.votes.Votes()
				allMoots = []
				outList = []

				cur.execute("SELECT * FROM moots ORDER BY moot_post_time DESC")
				result = cur.fetchall()

				for i in result:
					allMoots.append([votes.getMootScore(i["moot_id"]), i]) # create a 2D list of score and object
					# allMoots.sort()

					allMoots = sorted(allMoots,key=lambda x: (x[0])) # sort by the first collumn (score)
					allMoots = allMoots[::-1] # invert order of the list

					# print("\n", allMoots)

				if rangeEnd > 0:
					allMoots = allMoots[rangeStart:rangeEnd] # get only the selected moots
				else:
					allMoots = allMoots[rangeStart:99999999999999]
				
				# print("\n\nGetting all moots\n", allMoots)
				
				for i in allMoots: # create a second list with only the moot objects
					outList.append(i[1])
				return outList

			elif grabType == "user": # get all posts from a given user ID, sorting by when they were posted
				cur.execute("SELECT * FROM moots WHERE moot_owner = ? ORDER BY moot_post_time DESC", [grabData])
				result = cur.fetchall()
				result = result[rangeStart:rangeEnd]

			elif grabType == "search": # search for moots, given a set of keywords. I use python to do it, as I don't know how to searhc with multiple keywords in SQlite
				
				searchList = grabData.split(" ") # split search to a set of keywords
				# print(searchList)

				# cur.execute("SELECT * FROM moots ORDER BY moot_post_time DESC") # get all moots
				# allMoots = cur.fetchall()
				
				allMoots = self.grabMoots("score", "", 0, -1)[::-1] # grab all moots sorting by score, so moots with an equal number of keyword hits are sorted by how good they are

				mootArray = []
				for moot in allMoots:
					mootArray.append([0, moot]) # make 2D array of moots and the number of results they got
				

				for moot in mootArray: # iterate through the 2D list of moots, setting one collumn to the moot and the other to the number of keywords present in the title
					for query in searchList:
						moot[0] = moot[0] + moot[1]["moot_title"].lower().count(query.lower()) # add the number of times the current keyword appears to the moot's score


						# ---------------------
						# different algorithim: this system *doesn't* reward moots for repeating keywords, which could yield slightly different search results 
						# ---------------------
						# if moot[1]["moot_title"].find().count(query.lower()) != -1: 
							# moot[0] = moot[0] + 1


						# print(moot[1]["moot_title"], query) # find the number of results for each moot
				

				mootArray = sorted(mootArray,key=lambda x: (x[0])) # sort the 2D list by the first collumn (score). I got this code of stackoverflow, I couldn't work out how to do it efficently myself
				mootArray = mootArray[::-1] # invert order of the list, so highest hits goes first
				if rangeEnd > 0:
					mootArray = mootArray[rangeStart:rangeEnd] # get only the selected moots by the range limiter, in case I want to only show n moots at a given time
				else:
					mootArray = mootArray[rangeStart:99999999999999]
				


				outList = []
				for row in mootArray:
					if row[0] != 0:
						outList.append(row[1]) # create an outlist of exclusively moots, only showing ones that match at least one tag

				result = outList



				# --- OLD SINGLE QUERY CODE ---
				# grabData = "%" + grabData + "%"

				# cur.execute("SELECT * FROM moots WHERE moot_title LIKE ? ORDER BY moot_post_time DESC", [grabData])
				# result = cur.fetchall()
			else:
				return False

			conn.commit()
			cur.close()

			# if result:
			return result
			# else:
			# 	return False
			

	def deleteMoot(self, username, mootID): # delete a moot, and all linked votes, from the database. Takes username to validate if the user is allowed to do so (owner or admin)
		# print("\n\n", title, body, username)
		
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			user = classes.user.User()
			userID = user.getUser("username", username)
			# print(userID)
			userID = userID["user_id"]

			cur.execute("SELECT * FROM moots WHERE moot_id = ?", (mootID,))
			result = cur.fetchone()
			if result == None:
				return False # If no moot with that ID exists, tell the user there's a problem
			
			if result["moot_owner"] == userID or user.getUser("username", username)["user_admin"] == 1:

				# print(result["moot_id"])
				cur.execute("DELETE FROM moots WHERE moot_id = ?", [mootID]) # Delete the moot

				cur.execute("DELETE FROM voteLinker WHERE moot_id = ?", [mootID]) # Delete all votes on the moot as well
			
			conn.commit()
			cur.close()

			return True


	def editMoot(self, username, mootID, newTitle, newBody): # Edit a moot to contain new data. The form is autofilled with the old data when the page loads, this just needs to check ownership and update the DB
		
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()


			user = classes.user.User()
			userID = user.getUser("username", username)
			# print(userID)
			userID = userID["user_id"]

			cur.execute("SELECT * FROM moots WHERE moot_owner = ? AND moot_id = ?", [userID, mootID])
			result = cur.fetchone() # Check the moot exists and the current user owns it
			if result == None:
				return False
			

			result = cur.execute("UPDATE moots SET moot_title = ?, moot_body = ?, moot_edited = 1 WHERE moot_id = ?", [newTitle, newBody, mootID]) # Update the moot with new info, also set a not saying it's been edited
			
			# result = cur.execute("INSERT INTO moots (moot_owner, moot_edited, moot_title, moot_body, moot_post_time) VALUES (?, 0, ?, ?, ?)", (userID, title, body, time.time()))
			# print(userID, title, body)
			
			conn.commit()
			cur.close()

			if result:
				return True
			else:
				return False


	def getMootOwner(self, mootID):	# Get the owner (userID) of a given moot
		with sqlite3.connect(self.dbPath) as conn:
			conn.row_factory = sqlite3.Row #change sqlite to give back dictionaries, not tuples
			cur = conn.cursor()

			# user = classes.user.User()
			# userID = user.getUser("username", username)
			# print(userID)
			# userID = userID["user_id"]

			# result = cur.execute("INSERT INTO moots (moot_owner, moot_edited, moot_title, moot_body, moot_post_time) VALUES (?, 0, ?, ?, ?)", (userID, title, body, time.time()))
			# # print(userID, title, body)
			

			cur.execute("SELECT * FROM moots WHERE moot_id = ?", [mootID])
			result = cur.fetchone()

			conn.commit()
			cur.close()

			if result:
				return result["moot_owner"]
			else:
				return False