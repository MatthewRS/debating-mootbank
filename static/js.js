$(document).ready(function(){

	// alert("Hi");


	// Below is tag adding stuff (bascially magic)

	$(".tagItem").click(function(){ // Adding logic

        let tagField = $('#tags');
        let previousFields = tagField.val();
        let thisTag = this.id; // get the tag from the button
        // console.log(thisTag);
        let newFields = previousFields+', '+thisTag; // append the new tag onto the current list

        if (newFields.charAt(0) == ',') {
            newFields = newFields.substring(1);
        }

        tagField.val(newFields);

        $(this).fadeOut(120); // Fade out the old tag button
    });



    $(".tagItem").hover(function(){ // Make a clicky cursor on hover
        document.body.style.cursor = "pointer";
    });

	
})